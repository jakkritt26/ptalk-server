import * as mongoose from "mongoose";
import { Document } from "mongoose";

export const UsersSchema = new mongoose.Schema({
  email: String,
  password: String,
  name: String,
  lastname: String,
  telephone: String,
  country: Number,
  role: String,
  pro: Number,
  pro_new_end: { type: Date },
  pro_end: { type: Date },
  facebook_login: String,
  google_login: String,
  line_login: String,
  apple_login: String,
  // follow_user: {},
  create_at: { type: Date, default: Date.now },
  isActive: { type: Boolean, default: true },
});

export interface Users extends Document {
  readonly email: string;
  readonly password: string;
}
