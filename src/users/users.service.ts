import { Injectable } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { Model } from 'mongoose';
import { CreateUsersDto } from './dto/create-user.dto';
import { Users } from './schema/users.schema';

@Injectable()
export class UsersService {
  constructor(
    @InjectModel('Users') private readonly usersModel: Model<Users>,
  ) {}

  async create(createUsersDto: CreateUsersDto): Promise<any> {
    const createdUsers = new this.usersModel(createUsersDto);
    console.log(createdUsers);
    return await createdUsers.save();
  }
}
